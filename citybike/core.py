import requests
from dictor import dictor


def get_data(url, items):
    """ method for accessing data set and parsing out data to relational compatible format
    It is leveraging dictor for parsing instead of hard-coding
    :param url: the url data
    :param items: the json keys that need to be parsed out
    :return: parsed records
    """
    response = requests.get(url=url)
    print('response code for %s: %s', url, response.status_code)
    # print(json.dumps(response.json(), ensure_ascii=False))
    response_json = response.json()
    execution_time = response_json['executionTime']
    for station in response_json['stationBeanList']:
        parsed_record = [dictor(station, item) for item in items]
        parsed_record.append(execution_time)
        # print(parsed_record)
        yield parsed_record
