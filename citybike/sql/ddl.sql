drop table if exists citibikenyc cascade
;

create table citibikenyc (
    "id" int,
    "stationName" varchar(128),
    "availableDocks" int,
    "totalDocks" int,
    "testStation" boolean,
    "lastCommunicationTime" timestamp,
    "execution_time" timestamp
)
;