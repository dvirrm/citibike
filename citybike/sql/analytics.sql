CREATE OR REPLACE VIEW citibikenyc_health AS

SELECT *,
       "totalDocks" - "availableDocks"                                     AS
       brokenDockes,
       Extract(epoch FROM execution_time - "lastCommunicationTime") / 3600 AS
       hoursSinceLastComm,
       CASE
         WHEN ( "totalDocks" - "availableDocks" ) < 0 THEN 'bad record'
         WHEN ( "totalDocks" - "availableDocks" ) < 10 THEN 'green'
         WHEN ( "totalDocks" - "availableDocks" ) < 30 THEN 'yellow'
         ELSE 'red'
       end                                                                 AS
       stationHealth
FROM   citibikenyc c
WHERE  NOT "testStation"
;