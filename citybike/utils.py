import csv
import psycopg2


def write_data(filename, data_function, **kwargs):
    """
    method for writing data to file multirows
    :param filename: output file name
    :param data_function: the function that handles the data
    :param kwargs: params for the data_function
    """
    with open(filename, encoding='utf-8', newline='\n', mode='w') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerows(data_function(**kwargs))


def exec_sql(sql, uri):
    """" executes postgresql sql
    :param uri: connection string
    :param sql: sql command to execute"""
    conn = psycopg2.connect(uri)
    conn.autocommit = True
    curr = conn.cursor()
    curr.execute(sql)
    conn.commit()
    conn.close()


def select_sql(sql, uri):
    """" executes postgresql sql
    :param uri: connection string
    :param sql: sql command to execute"""
    conn = psycopg2.connect(uri)
    curr = conn.cursor()
    curr.execute(sql)
    result = curr.fetchall()
    print(result)
    return result
    conn.close()
