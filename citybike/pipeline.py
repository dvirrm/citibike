import json
from citybike import core
from citybike import utils
import os
from jinja2 import Template
from dotenv import load_dotenv, dotenv_values

load_dotenv()
URI = dotenv_values()['uri']

if __name__ != "__main__":
    exit()

with open('config.json', 'r') as config_file:
    config = json.load(config_file)

CITIBIKENYC_URL = config['url']
ITEMS = config['items']
FILENAME = 'output/citybikenyc.csv'


utils.write_data(filename=FILENAME,
                 url=CITIBIKENYC_URL,
                 items=ITEMS,
                 data_function=core.get_data)


sql_steps = config['sql']
path = os.path.dirname(os.path.abspath(__file__))

for step in sql_steps:
    print('executing {}'.format(step['step name']))
    sql = open(step['sql script'], 'r').read()
    templated_sql = Template(sql).render(path=path,
                                         file=FILENAME)
    utils.exec_sql(sql=templated_sql,
                   uri=URI)


os.remove('{}/{}'.format(path, FILENAME))
