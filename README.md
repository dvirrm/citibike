# Methodology
### Data processing
The task's data layer is a relational database. \
The data processing stage is being handled in the db itself, \
so that the pipeline focuses on handling the data ingestion and parsing.
### Increment handling and time-dimension
The way I see it the data is actually a snapshot of a state, \
so the api makes a naive call and gets the state. \
The data point is `executionTime` and not a business time. \
So for ongoing process the api would not accept time parameter, \
but the execution time captured will increase up as executions occure.

# Project structure
Consists of the following
* config `config.json` \
the configurations are handled there, so there would be no need to change code
* `core.py`
The core function for the task in hand
* `pipeline.py` executor of the data pipeline
* `utils.py` additional functions for general usage
* `sql/` The directory holds the necessary SQL scripts

# Usage
The project was done with the following stack:
* Windows OS
* Python 3
* PostgreSQL 11 

Please refer to `requirements.txt` for packages in use \
Please refer to `.env` for database uri \
Please keep in mind explicit allow for the `/output` directory might be needed for Postgresql could access and ingest the file

# Pipeline
The pipeline revolves around configuration file and includes the following stages:
### Data dump
Done with simple api call, dumped to local. 
### Data ingestion
Done with Postgresql COPY command
### Data processing
Done by creating a view. 
* test stations are excluded 
* Station health check is based on the provided rules. 
* Added rule for `bad record` if available docks < total docks 
* Added indicator of diff (execution time - last communication time), in hours for additional monitoring
### Cleanup
Once all is done remove local file
